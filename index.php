<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Your order's</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    
    <body>
        <?php   
            echo "<h1>Thanks for your order. Here it is:</h1><br>";
         
            //Regular Form         
            for($i=1;$i<=$_POST["numCoffee"];$i++){
                if (($_POST["size"]=="small")&&($_POST["numCoffee"]>0)){
                    echo '<img src="images/cup.jpg" width="200" height="250">';
            }
            }
        
            
             for($i=1;$i<=$_POST["numCoffee"];$i++){
                if (($_POST["size"]=="medium")&&($_POST["numCoffee"]>0)){
                    echo '<img src="images/cup.jpg" width="220" height="270">';
            }
            }
            
             for($i=1;$i<=$_POST["numCoffee"];$i++){
                if (($_POST["size"]=="large")&&($_POST["numCoffee"]>0)){
                    echo '<img src="images/cup.jpg" width="240" height="290">';
            }
            }
            
             for($i=1;$i<=$_POST["numCoffee"];$i++){
                if (($_POST["size"]=="xlarge")&&($_POST["numCoffee"]>0)){
                    echo '<img src="images/cup.jpg" width="260" height="333">';
            }
            }
            
            if ($_POST["numSugar"]>0){
                echo '<img src="images/plus.jpg">';
            }
         
            for($i=1;$i<=$_POST["numSugar"];$i++){
                echo '<img src="images/sugar.jpg">';
            }
            
            if ($_POST["numCream"]>0){
                echo '<img src="images/plus.jpg">';
            }
            
            for($i=1;$i<=$_POST["numCream"];$i++){
                echo '<img src="images/cream.jpg">';
            }
                 
            //Slang Form
            if(($_POST["slang"]=="regular")&&($_POST["size"]=="small")){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';                
                echo '<img src="images/cream.jpg">';              
            }
            
            if(($_POST["slang"]=="regular")&&($_POST["size"]=="medium")){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';                
                echo '<img src="images/cream.jpg">'; 
                echo '<img src="images/cream.jpg">';
            }
            
            if(($_POST["slang"]=="regular")&&($_POST["size"]=="large")){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';                
                echo '<img src="images/cream.jpg">';   
                echo '<img src="images/cream.jpg">'; 
                echo '<img src="images/cream.jpg">'; 
            }
            
            if(($_POST["slang"]=="regular")&&($_POST["size"]=="xlarge")){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';                
                echo '<img src="images/cream.jpg">';              
                echo '<img src="images/cream.jpg">';   
                echo '<img src="images/cream.jpg">';   
                echo '<img src="images/cream.jpg">';   
            }
            
            
            
            if($_POST["slang"]=="double"){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';   
                echo '<img src="images/cream.jpg">';
                echo '<img src="images/cream.jpg">';   
            }
         
            if($_POST["slang"]=="triple"){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/plus.jpg">';  
                echo '<img src="images/cream.jpg">';
                echo '<img src="images/cream.jpg">';   
                echo '<img src="images/cream.jpg">';
            }
            
            if($_POST["slang"]=="blackOne"){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                
            }
            
            if($_POST["slang"]=="blackTwo"){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';               
            }
            
            if($_POST["slang"]=="blackThree"){
                echo '<img src="images/plus.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
                echo '<img src="images/sugar.jpg">';
            }
            echo "<br>";
         
         //Cost
            setlocale(LC_MONETARY,"en_CA");
         
            if($_POST["size"]=="small"){
                $cost= (1.59*1.13*$_POST["numCoffee"]);   
                echo money_format("<h3>Cost: $1.59 * {$_POST["numCoffee"]} + tax = %i</h3>",$cost); 
            }
         
            if($_POST["size"]=="medium"){
                $cost= (1.79*1.13*$_POST["numCoffee"]);   
                echo money_format ("<h3>Cost: $1.79 * {$_POST["numCoffee"]} + tax = %i</h3>", $cost);          
            }
         
            if($_POST["size"]=="large"){
                $cost= (1.99*1.13*$_POST["numCoffee"]);   
                echo money_format ("<h3>Cost: $1.99 * {$_POST["numCoffee"]} + tax = %i</h3>", $cost); 
            }
         
            if($_POST["size"]=="xlarge"){
                $cost= (2.19*1.13*$_POST["numCoffee"]);  
                echo money_format ("<h3>Cost: $2.19 * {$_POST["numCoffee"]} + tax = %i</h3>" ,$cost);             
            }
      
        ?>
    </body>
</html>
